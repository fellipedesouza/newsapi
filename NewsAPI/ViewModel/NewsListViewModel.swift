//
//  NewsListViewModel.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation

struct NewsListViewModel {
    let news: [News]
}

extension NewsListViewModel {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.news.count
    }
    
    func newsAtIndex(_ index: Int) -> NewsViewModel {
        let news = self.news[index]
        return NewsViewModel(news)
    }
}
