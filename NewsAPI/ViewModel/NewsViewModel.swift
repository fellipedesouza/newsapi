//
//  NewsViewModel.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation

struct NewsViewModel {
    private let news: News
}

extension NewsViewModel {
    init(_ news: News) {
        self.news = news
    }
}

extension NewsViewModel {
    var title: String {
        return self.news.title
    }
    
    var description: String {
        return self.news.description
    }
}
