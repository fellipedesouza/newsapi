//
//  NewsList.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation

struct NewsList: Decodable {
    let data: [News]
}
