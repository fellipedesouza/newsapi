//
//  News.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation

struct News: Decodable {
    let title: String
    let description: String
}
