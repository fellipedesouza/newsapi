//
//  NewsRequest.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation

class NewsRequest {
    
    private let url: URL = URL(string: "http://api.mediastack.com/v1/news?access_key=30876adc93fe4d4ff6124a4770576b48")!
    
    func getNews(completion: @escaping ([News]?) -> ()) {
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                
                let newsData = try? JSONDecoder().decode(NewsList.self, from: data)
                
                if let newsData = newsData {
                    completion(newsData.data)
                }
            }
        }.resume()
    }
}
