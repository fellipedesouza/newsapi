//
//  NewsTableViewCell.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation
import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
