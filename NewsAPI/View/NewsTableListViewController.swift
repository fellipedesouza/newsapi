//
//  NewsTableListViewController.swift
//  NewsAPI
//
//  Created by Fellipe de Souza on 15/11/21.
//

import Foundation
import UIKit

class NewsTableListViewController: UITableViewController {
    
    private var newsListViewModel: NewsListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        fetchNews()
    }
    
    private func fetchNews() {
        NewsRequest().getNews() { news in
            if let news = news {
                self.newsListViewModel = NewsListViewModel(news: news)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.newsListViewModel == nil ? 0 : self.newsListViewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsListViewModel.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCellIdentifier = "NewsTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath)
                as? NewsTableViewCell else {
            fatalError("NewsTableViewCell not found")
        }
        
        let newsViewModel = self.newsListViewModel.newsAtIndex(indexPath.row)
        
        cell.titleLabel.text = newsViewModel.title
        cell.descriptionLabel.text = newsViewModel.description
        
        return cell
    }
}
